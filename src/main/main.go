package main 

import (
    "runtime"
	"github.com/go-gl/gl/v2.1/gl"
	"github.com/go-gl/glfw/v3.1/glfw"
	"time"
	"strconv"
)
func init() {
    // This is needed to arrange that main() runs on main thread.
    // See documentation for functions that are only allowed to be called from the main thread.
    runtime.LockOSThread()
}
type StopWatch struct {
	start time.Time
	stop time.Time
}
func (stopWatch *StopWatch)Start() {
	stopWatch.start = time.Now().UTC()
}
func (stopWatch *StopWatch)Stop() {
	stopWatch.stop = time.Now().UTC()
}
func (stopWatch *StopWatch)GetDuration() time.Duration {
	return stopWatch.stop.Sub(stopWatch.start)
}

func (stopWatch *StopWatch)GetFPS() int64 {
	return 1000.0/(1+(stopWatch.GetDuration().Nanoseconds()/1e6))
}
var width int = 1920
var height int = 1080
var stopWatch *StopWatch = new(StopWatch)


var rotationX float32
var rotationY float32

func main() {
	err := glfw.Init()
    if err != nil {
        panic(err)
    }
    defer glfw.Terminate()

    window, err := glfw.CreateWindow(width, height, "Testing", nil, nil)
    if err != nil {
        panic(err)
    }
    window.MakeContextCurrent()
	glfw.SwapInterval(0) // VSync off

	if err := gl.Init(); err != nil {
        panic(err)
    }
	gl.ClearColor(0.0, 0.0, 0.3, 0.0)
	setupScene()
    for !window.ShouldClose() {
    	stopWatch.Start()
		
		render()
		
        window.SwapBuffers()
        glfw.PollEvents()
    	stopWatch.Stop()
    	window.SetTitle(strconv.FormatInt(stopWatch.GetDuration().Nanoseconds()/1e6, 10) + " ms | " + strconv.FormatInt(stopWatch.GetFPS(), 10))
    }
}
func setupScene() {
	gl.Enable(gl.DEPTH_TEST)
	gl.Enable(gl.LIGHTING)

	gl.ClearColor(0.5, 0.5, 0.5, 0.0)
	gl.ClearDepth(1)
	gl.DepthFunc(gl.LEQUAL)

	ambient := []float32{0.5, 0.5, 0.5, 1}
	diffuse := []float32{1, 1, 1, 1}
	lightPosition := []float32{-5, 5, 10, 0}
	gl.Lightfv(gl.LIGHT0, gl.AMBIENT, &ambient[0])
	gl.Lightfv(gl.LIGHT0, gl.DIFFUSE, &diffuse[0])
	gl.Lightfv(gl.LIGHT0, gl.POSITION, &lightPosition[0])
	gl.Enable(gl.LIGHT0)

	gl.MatrixMode(gl.PROJECTION)
	gl.LoadIdentity()
	gl.Frustum(-1, 1, -1, 1, 1.0, 10.0)
	gl.MatrixMode(gl.MODELVIEW)
	gl.LoadIdentity()
}
func render() {
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	gl.MatrixMode(gl.MODELVIEW)
	gl.LoadIdentity()
	gl.Translatef(0, 0, -3.0)
	gl.Rotatef(rotationX, 1, 0, 0)
	gl.Rotatef(rotationY, 0, 1, 0)
	
	rotationX += 0.005
	rotationY += 0.005
	
	gl.Color4f(1, 1, 1, 1)

	gl.Begin(gl.QUADS)

	gl.Normal3f(0, 0, 1)
	gl.Vertex3f(-1, -1, 1)
	gl.Vertex3f(1, -1, 1)
	gl.Vertex3f(1, 1, 1)
	gl.Vertex3f(-1, 1, 1)

	gl.Normal3f(0, 0, -1)
	gl.Vertex3f(-1, -1, -1)
	gl.Vertex3f(-1, 1, -1)
	gl.Vertex3f(1, 1, -1)
	gl.Vertex3f(1, -1, -1)

	gl.Normal3f(0, 1, 0)
	gl.Vertex3f(-1, 1, -1)
	gl.Vertex3f(-1, 1, 1)
	gl.Vertex3f(1, 1, 1)
	gl.Vertex3f(1, 1, -1)

	gl.Normal3f(0, -1, 0)
	gl.Vertex3f(-1, -1, -1)
	gl.Vertex3f(1, -1, -1)
	gl.Vertex3f(1, -1, 1)
	gl.Vertex3f(-1, -1, 1)

	gl.Normal3f(1, 0, 0)
	gl.Vertex3f(1, -1, -1)
	gl.Vertex3f(1, 1, -1)
	gl.Vertex3f(1, 1, 1)
	gl.Vertex3f(1, -1, 1)

	gl.Normal3f(-1, 0, 0)
	gl.Vertex3f(-1, -1, -1)
	gl.Vertex3f(-1, -1, 1)
	gl.Vertex3f(-1, 1, 1)
	gl.Vertex3f(-1, 1, -1)

	gl.End()
}

