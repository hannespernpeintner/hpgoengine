package util

import (
	"time"
	"fmt"
)
type StopWatch struct {
	start int64
	stop int64
}
func (stopWatch *StopWatch)Start() int64 {
	stopWatch.start = time.Now().UnixNano()
	return stopWatch.start
}
func (stopWatch *StopWatch)Stop() int64 {
	stopWatch.stop = time.Now().UnixNano()
	return stopWatch.stop
}
func (stopWatch *StopWatch)GetTimeSpanInNS() int64 {
	return stopWatch.stop - stopWatch.start
}
func (stopWatch *StopWatch)GetTimeSpanInMS() int64 {
	return stopWatch.GetTimeSpanInNS()/1000
}
func (stopWatch *StopWatch)PrintTimeSpanInMs() {
	fmt.Println(string(stopWatch.GetTimeSpanInMS()))
}